/**
 * Error-handler function module.
 * @module helpers/error
 */

import { NextFunction } from 'express';

/** Error messages to be thrown. */
export const errorMessage = {
  BAD_REQUEST: 'Bad Request',
  FORBIDDEN: 'Forbidden',
  UNSUPPORT_CONTENT_TYPE: 'Unsupported Media Type',
  NOT_FOUND: 'Not Found',
  VALIDATION_FAIL: 'Validation failed',
  INVALID_JWT: 'Invalid JWT',
  DUPLICATE_KEY: 'Duplicate item exists on database',
  DATABASE_ERROR: 'Database Error',
  INTERNAL_ERROR: 'Internal Server Error',
  EXTERNAL_ERROR: 'ExternalError',
  OTP_FAIL: 'Invalid otp',
  RATE_LIMIT_FAIL: 'Too many request',
  INVALID_TOKEN: 'Invalid token',
  UNAUTHORIZED: 'Unauthorized user',
  INVALID_RESET_PASSWORD_TOKEN: 'Invalid reset password token',
  EXPIRED_TOKEN: 'Expired token',
  INVALID_PROJECT_ID: 'Invalid project id',
  PRODUCT_NOT_FOUND: 'Product not found',
};

export class CustomError extends Error {
  status: number;
  simplifiedMessage: string;
  context: any;
  constructor({
    status,
    message,
    simplifiedMessage,
    context,
  }: {
    status: number;
    message: string;
    simplifiedMessage?: string;
    context?: any;
  }) {
    super(message);
    this.status = status;
    this.simplifiedMessage = simplifiedMessage ? simplifiedMessage : message;
    this.context = context;
  }
}

export function badRequest(error: Error, next?: NextFunction) {
  const _error = new CustomError({
    status: 400,
    message: error.message,
    simplifiedMessage: errorMessage.BAD_REQUEST,
  });

  /* istanbul ignore else */
  if (typeof next === 'function') next(_error);
  else throw _error;
}

export function internalError(
  error: Error,
  simplifiedMessage: string = errorMessage.INTERNAL_ERROR,
  next?: NextFunction,
) {
  const _error = new CustomError({
    status: 500,
    message: error.message,
    simplifiedMessage: simplifiedMessage,
  });

  /* istanbul ignore else */
  if (typeof next === 'function') next(_error);
  else throw error;
}

export function externalError(
  error: Error,
  simplifiedMessage: string = errorMessage.EXTERNAL_ERROR,
  next?: NextFunction,
) {
  const _error = new CustomError({
    status: 500,
    message: error.message,
    simplifiedMessage: simplifiedMessage,
  });

  /* istanbul ignore else */
  if (typeof next === 'function') next(_error);
  else throw error;
}

export function notFound(next: NextFunction) {
  const error = new CustomError({
    status: 404,
    message: errorMessage.NOT_FOUND,
  });

  /* istanbul ignore next */
  if (typeof next === 'function') next(error);
  else throw error;
}

export function unsupportContentType(next?: NextFunction) {
  const error = new CustomError({
    status: 415,
    message: errorMessage.UNSUPPORT_CONTENT_TYPE,
  });

  /* istanbul ignore else */
  if (typeof next === 'function') next(error);
  else throw error;
}
