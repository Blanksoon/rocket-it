import { Request, Response } from 'express';
import { ApiResponse } from '../types/api-type';

export function send(payload: ApiResponse, req: Request, res: Response) {
  res.status(payload.code);
  res.send(payload);
}
