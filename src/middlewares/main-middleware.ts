/**
 * Main middleware.
 * @module middlewares/main
 */

import * as express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import multer from 'multer';

/**
 * Set middlewares to app instance.
 * @param app - Express' app instance.
 */
export default function (app: express.Application) {
  app.use(helmet());
  app.use(cors({ origin: process.env.FRONTEND_URL, credentials: true }));
  app.use(compression());
  app.use(cookieParser());
  app.use(bodyParser.json({ limit: process.env.MAX_BODY_SIZE }));
  app.use(bodyParser.urlencoded({ extended: false, limit: process.env.MAX_BODY_SIZE }));
  app.use(
    multer({
      dest: './tmp',
      limits: {
        fileSize: parseInt(process.env.FILEUPLOAD_MAX_FILESIZE),
        files: parseInt(process.env.FILEUPLOAD_MAX_FILES),
        fields: 0,
      },
    }).single('upload'),
  );
}
