/**
 * Routers middleware.
 * @module middlewares/route
 */

import * as express from 'express';
import routes from '../routes/routes';

/**
 * Attach routing module to app instance.
 * @param app - Express' app instance.
 * @param express - Express package itself
 */
export default function (app: express.Application) {
  // Main router
  app.use('/', routes);
}
