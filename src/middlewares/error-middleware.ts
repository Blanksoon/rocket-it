/**
 * Error handler middleware.
 * @module middlewares/error
 */

import * as express from 'express';
import logger from '../helpers/logger';
import { send } from '../helpers/response';
import { notFound } from '../helpers/error';

/**
 * Set error handler to app.
 * @param {Object} app - Express' app instance.
 */
export default function (app: express.Application) {
  // Catch request if not match in any route and mark as 404
  app.use((req, res, next) => notFound(next));

  // This middleware requires 4 parameters to be able to be identified as error handler.
  app.use((error: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    /* istanbul ignore next */
    const status = error.status || 500;

    // There is an application error, save detailed log.
    if (status >= 500) {
      // If it is not simplified from previous middleware so it is some uncaught error.
      /* istanbul ignore if */
      if (typeof error.simplifiedMessage === 'undefined') {
        error.message = `${error.message} [UNCAUGHT]`; // eslint-disable-line no-param-reassign
        error.simplifiedMessage = 'Internal error.'; // eslint-disable-line no-param-reassign
      }

      logger.error('[SERVER ERROR] ', {
        status: status,
        message: error.message,
        simplifiedMessage: error.simplifiedMessage,
        stack: error.stack,
        context: error.context,
      });
    } else {
      error.status = status;
      error.simplifiedMessage = error.simplifiedMessage || 'request error';
      logger.warn('[CLIENT ERROR] ', {
        status: status,
        message: error.message,
        context: error.context,
      });
    }
    send(
      {
        code: status,
        errorMessage: error.simplifiedMessage,
      },
      req,
      res,
    );
    next();
  });
}
