import * as express from 'express';
import { sendSuggestionByUserId } from '../controllers/suggestions-controller';
const router = express.Router({ mergeParams: true });

router.post('/send-suggestion/:userId', sendSuggestionByUserId.handler);

export default router;
