import * as express from 'express';
import transactionRoutes from './transactions';
import suggesstionRoutes from './suggesstions';

const router = express.Router();

router.use('/transactions', transactionRoutes);
router.use('/suggestions', suggesstionRoutes);

export default router;
