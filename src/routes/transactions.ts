import * as express from 'express';
import {
  getTransactionsByUserIdController,
  getMostPopularCatagoryByUserId,
} from '../controllers/transactions-controller';

const router = express.Router({ mergeParams: true });

router.get('/get-by-userId/:userId', getTransactionsByUserIdController.handler);
router.get('/get-most-popular-category/:userId', getMostPopularCatagoryByUserId.handler);

export default router;
