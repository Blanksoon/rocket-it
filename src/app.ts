import express from 'express';
import dotenv from 'dotenv';
import logMiddleware from './middlewares/log-middleware';
import mainMiddleware from './middlewares/main-middleware';
import routeMiddleware from './middlewares/route-middleware';
import errorMiddleware from './middlewares/error-middleware';

// Load environment variable
const envFilePath = `.env.${process.env.NODE_ENV || 'develop'}`;
dotenv.config({ path: envFilePath });

const app = express();

mainMiddleware(app);
logMiddleware(app);
routeMiddleware(app);
errorMiddleware(app);

export default app;
