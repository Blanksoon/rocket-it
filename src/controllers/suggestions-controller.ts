import * as Express from 'express';
import moment from 'moment';
import { createHandler } from './helpers';
import { send } from '../helpers/response';
import { badRequest, internalError } from '../helpers/error';
import { getUser } from '../data/users';
import { getTransactionsRangeByUserId } from '../data/transactions';
import { getProductsWithoutIds } from '../data/products';
import { findMostFrequentCategory } from '../services/transactions';

export const sendSuggestionByUserId = {
  handler: createHandler(
    async (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
      const { userId } = req.params;
      const { asOfDate } = req.query;
      try {
        const user = getUser(userId);
        if (!user) {
          throw internalError(new Error('user not found'), '', next);
        }
        if (!asOfDate) {
          throw badRequest(new Error('asOfDate is required.'), next);
        }

        const _asOfDate = String(asOfDate);
        const startDate = moment(_asOfDate).subtract(30, 'day').format('YYYY-MM-DD');
        const endDate = _asOfDate;
        let transactions = getTransactionsRangeByUserId(userId, startDate, endDate);

        if (transactions.length > 0) {
          const frequentCategory = findMostFrequentCategory(transactions);
          const productIds = [];
          transactions.map((transaction) => {
            const { transactionItems } = transaction;
            const _productIds = transactionItems
              .filter((transactionItem) => transactionItem.categoryId === frequentCategory)
              .map((transactionItem) => transactionItem.productId);

            productIds.push(..._productIds);
            return productIds;
          });
          const products = getProductsWithoutIds(productIds, frequentCategory);

          send({ code: 200, data: products }, req, res);
        } else {
          send({ code: 200, data: [] }, req, res);
        }
      } catch (error) {
        next(error);
      }
    },
  ),
};
