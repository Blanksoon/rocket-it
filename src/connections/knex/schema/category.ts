import { FieldDefinition, dateFields } from '../helpers';

const commonFields: FieldDefinition[] = [
  { name: 'name', type: 'string', unique: false, index: false, nullable: true },
  {
    name: 'status',
    type: 'enum',
    unique: false,
    index: false,
    nullable: false,
    enu: ['active', 'inactive', 'delete'],
  },
];

// this is for default table
export const schemas: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: true, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];

// this is for history table
export const schemash: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: false, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];
