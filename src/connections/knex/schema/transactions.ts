import { FieldDefinition, dateFields } from '../helpers';

const commonFields: FieldDefinition[] = [
  { name: 'userId', type: 'string', unique: false, index: true, nullable: false },
  { name: 'totalAmount', type: 'number', unique: false, index: false, nullable: false },
  { name: 'totalPrice', type: 'currency', unique: false, index: false, nullable: false },
  {
    name: 'status',
    type: 'enum',
    unique: false,
    index: false,
    nullable: false,
    enu: ['pending', 'success', 'fail', 'cancel'],
  },
];

// this is for default table
export const schemas: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: true, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];

// this is for history table
export const schemash: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: false, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];
