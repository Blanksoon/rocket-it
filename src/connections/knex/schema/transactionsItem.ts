import { FieldDefinition, dateFields } from '../helpers';

const commonFields: FieldDefinition[] = [
  { name: 'transId', type: 'string', unique: false, index: true, nullable: false },
  { name: 'productId', type: 'string', unique: false, index: true, nullable: false },
  { name: 'categoryId', type: 'string', unique: false, index: true, nullable: false },
  { name: 'amount', type: 'number', unique: false, index: false, nullable: false },
  { name: 'price', type: 'currency', unique: false, index: false, nullable: false },
  { name: 'totalPrice', type: 'currency', unique: false, index: false, nullable: false },
];

// this is for default table
export const schemas: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: true, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];

// this is for history table
export const schemash: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: false, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];
