import { FieldDefinition, dateFields } from '../helpers';

const commonFields: FieldDefinition[] = [
  { name: 'title', type: 'string', unique: false, index: false, nullable: true },
  { name: 'firstName', type: 'string', unique: false, index: true, nullable: false },
  { name: 'lastName', type: 'string', unique: false, index: true, nullable: false },
  { name: 'titleEn', type: 'string', unique: false, index: false, nullable: true },
  { name: 'firstNameEn', type: 'string', unique: false, index: true, nullable: true },
  { name: 'lastNameEn', type: 'string', unique: false, index: true, nullable: true },
  { name: 'birthDate', type: 'date', unique: false, index: false, nullable: false },
  { name: 'mobileNo', type: 'string', unique: false, index: false, nullable: true },
  { name: 'phoneNo', type: 'string', unique: false, index: false, nullable: true },
  {
    name: 'addressInfo',
    type: 'jsonb',
    unique: false,
    index: false,
    nullable: true,
    defaultTo: JSON.stringify({ shipping: {}, tax: {} }),
  },
  {
    name: 'status',
    type: 'enum',
    unique: false,
    index: false,
    nullable: false,
    enu: ['active', 'inactive', 'delete'],
  },
];

// this is for user table
export const schemas: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: true, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];

// this is for user history table
export const schemash: FieldDefinition[] = [
  { name: 'id', type: 'string', unique: false, index: true, nullable: false },
  ...commonFields,
  ...dateFields,
];
