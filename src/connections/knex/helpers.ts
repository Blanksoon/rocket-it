export interface FieldDefinition {
  name: string;
  type:
    | 'enum'
    | 'string'
    | 'text'
    | 'date'
    | 'timestamp'
    | 'jsonb'
    | 'percent'
    | 'currency'
    | 'number'
    | 'bigint'
    | 'boolean'
    | 'tstzrange';
  unique?: boolean;
  index?: boolean;
  nullable?: boolean;
  enu?: any[];
  defaultTo?: any;
  fk?: string;
}

export const dateFields: FieldDefinition[] = [
  { name: 'createDate', type: 'timestamp', unique: false, index: true, nullable: false },
  { name: 'createBy', type: 'string', unique: false, index: true, nullable: false },
  { name: 'lastModifyDate', type: 'timestamp', unique: false, index: true, nullable: false },
  { name: 'lastModifyBy', type: 'string', unique: false, index: true, nullable: false },
];
