export function findMostFrequentCategory(transactions) {
  let result = {};
  transactions.forEach((transaction) => {
    const { transactionItems } = transaction;
    transactionItems.forEach((transactionItem) => {
      const { categoryId } = transactionItem;
      if (result[categoryId]) {
        result[categoryId] += transactionItem.amount;
      } else {
        result[categoryId] = transactionItem.amount;
      }
    });
  });

  return Object.keys(result).reduce((a, b) => (result[a] > result[b] ? a : b));
}
