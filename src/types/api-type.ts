export interface ApiResponse {
  code: number;
  data?: object;
  errorMessage?: string;
}
