export const mockUsers = [
  {
    id: 'u01',
    title: 'นาย',
    firstName: 'สมศรี',
    lastName: 'สมศรี',
    titleEn: 'Mr.',
    firstNameEn: 'Somsri',
    lastNameEn: 'Somsri',
    birthDate: '2000-01-01',
    mobileNo: '0987654321',
    phoneNo: '0123456789',
    addressInfo: {
      shipping: {},
      tax: {},
    },
    status: 'active',
    createDate: '2023-01-01 15:18:46.303+00',
    createBy: 'system',
    lastModifyDate: '2023-01-01 15:18:46.303+00',
    lastModifyBy: 'system',
  },
];

export function getUser(userId) {
  return mockUsers.find((user) => user.id === userId);
}
