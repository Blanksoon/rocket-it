export const mockCategory = [
  {
    id: 'c01',
    name: 'Computer',
    status: 'active',
    createDate: '2023-01-01 15:18:46.303+00',
    createBy: 'system',
    lastModifyDate: '2023-01-01 15:18:46.303+00',
    lastModifyBy: 'system',
  },
  {
    id: 'c02',
    name: 'Mobile',
    status: 'active',
    createDate: '2023-01-01 15:18:46.303+00',
    createBy: 'system',
    lastModifyDate: '2023-01-01 15:18:46.303+00',
    lastModifyBy: 'system',
  },
  {
    id: 'c03',
    name: 'Television',
    status: 'active',
    createDate: '2023-01-01 15:18:46.303+00',
    createBy: 'system',
    lastModifyDate: '2023-01-01 15:18:46.303+00',
    lastModifyBy: 'system',
  },
];
