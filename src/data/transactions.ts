import { mockTransactionItems } from './transactionItems';

export const mockTransactions = [
  {
    id: 'a0000001',
    userId: 'u01',
    totalAmount: 16,
    totalPrice: 55900000,
    status: 'success',
    createDate: '2024-01-01 15:18:46.303+00',
    createBy: 'system',
    lastModifyDate: '2024-01-01 15:18:46.303+00',
    lastModifyBy: 'system',
  },
];

export function getTransactionsByUserId(userId) {
  const transactions = mockTransactions.filter((transaction) => transaction.userId === userId);
  const result = transactions.map((transaction) => {
    const { id } = transaction;
    const transactionItems = mockTransactionItems.filter(
      (transactionItem) => transactionItem.transId === id,
    );
    return {
      ...transaction,
      transactionItems,
    };
  });
  return result;
}

export function getTransactionsRangeByUserId(userId, startDate, endDate) {
  const transactions = mockTransactions
    .filter((tran) => tran.userId === userId)
    .filter((tran) => tran.createDate >= startDate && tran.createDate <= endDate);
  const result = transactions.map((transaction) => {
    const { id } = transaction;
    const transactionItems = mockTransactionItems.filter(
      (transactionItem) => transactionItem.transId === id,
    );
    return {
      ...transaction,
      transactionItems,
    };
  });
  return result;
}
