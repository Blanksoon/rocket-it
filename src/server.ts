import http from 'http';
import app from './app';

/**
 * @param {string} val - input port to be converted
 * @return {(string|number)} A number if val can be converted to number,
 *  val otherwise
 */
function normalizePort(val: string): number | string {
  const parsedPort = parseInt(val, 10);
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(parsedPort)) {
    return val;
  }
  return parsedPort;
}

function onError(error: NodeJS.ErrnoException) {
  if (error.syscall !== 'listen') {
    throw error;
  }
  // eslint-disable-next-line no-use-before-define
  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;
  switch (error.code) {
    case 'EACCES':
      console.error(`[APP] ${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`[APP] ${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  // eslint-disable-next-line no-use-before-define
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`[APP] Listening on ${bind}`);
}

const port = normalizePort(process.env.LISTEN_PORT || '3000');
app.set('port', port);

const server = http.createServer(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
